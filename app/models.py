from app import db
from config import DEFAULT_LOCATION_LATITUDE, DEFAULT_LOCATION_LONGITUDE

ROLE_USER = 0
ROLE_ADMIN = 1

# Association table for the Movie-Location many-to-many relationship.
movie_locations = db.Table('movie_locations',
                           db.Column('movie_id', db.Integer, db.ForeignKey('movie.id')),
                           db.Column('location_id', db.Integer, db.ForeignKey('location.id')))


class Movie(db.Model):
    """
    A movie from SFData's Film Locations in SF Database:
    https://data.sfgov.org/Arts-Culture-and-Recreation-/Film-Locations-in-San-Francisco/
    """
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), index=True, unique=False)
    release_year = db.Column(db.SmallInteger, default=9999)
    actor_1 = db.Column(db.String(64))
    actor_2 = db.Column(db.String(64))
    actor_3 = db.Column(db.String(64))
    writer = db.Column(db.String(64))
    director = db.Column(db.String(64))
    production_company = db.Column(db.String(64))
    distributor = db.Column(db.String(64))
    locations = db.relationship("Location", secondary=movie_locations,
                                backref=db.backref("movies", lazy="dynamic"))

    def __repr__(self):
        return self.title

class Location(db.Model):
    """
    A movie location.
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), unique=True)
    lat = db.Column(db.Float, default=DEFAULT_LOCATION_LATITUDE)
    lng = db.Column(db.Float, default=DEFAULT_LOCATION_LONGITUDE)
    fun_facts = db.Column(db.String(200))

    def __repr__(self):
        return "<%s>" % (self.name)