var splash = true;
var map;
var markers = [];
var infoWindow = null;

function initialize() {
    infoWindow = new google.maps.InfoWindow({
        content: null
    });
    var mapOptions = {
        center: mapCenterInit,
        zoom: 13,
        scaleControl: true
    };
map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions)
}

// Per official docs. Apparently maps object doesn't have a clear method.
function clearMarkers() {
    for (var i=0; i<markers.length; i++) {
        markers[i].setMap(null);
    }
    markers = [];
}

function makeMarkerText(name, facts) {
    return '<div id="content">' +
            '<div id="siteNotice"></div>' +
            '<h4>' + name + '</h4>' +
            '<div id="bodyContent"><p>' + facts + '</p>' +
            '</div>' +
            '</div>';
}

// Draw map markers given a a list of location dicts w/ position, facts, etc.
function mapLocations(data) {
    var locations = data.locations;
    var mapBounds = new google.maps.LatLngBounds();
    for (var i=0; i<locations.length; i++) {
        // Don't print "null".
        locations[i]["fun_facts"] == null ? locations[i]["fun_facts"] = "" : locations[i]["fun_facts"];
        var markerLocation = new google.maps.LatLng(locations[i]["lat"], locations[i]["lng"]);
        mapBounds.extend(markerLocation);
        var markerText = makeMarkerText(locations[i]["name"], locations[i]["fun_facts"]);
        var marker = new google.maps.Marker({
            map: map,
            position: markerLocation,
            html: markerText
        });
        // Plotting multiple markers... need to attach a callback to each marker. JavaScript!
        // See http://you.arenot.me/2010/06/29/google-maps-api-v3-0-multiple-markers-multiple-infowindows/
        google.maps.event.addListener(marker, 'click', function() {
            infoWindow.setContent(this.html);
            infoWindow.open(map, this);
        });
        markers.push(marker);
    }
    map.fitBounds(mapBounds);
    // Handle situation where there's one marker or few close together and map auto-zooms super tight.
    var listener = google.maps.event.addListener(map, "idle", function() {
      if (map.getZoom() > 16) map.setZoom(16);
      google.maps.event.removeListener(listener);
    });
}

function populateMovieInfo(data) {
    console.log(data.title);
    $("#movie-title").text(data.title);
    $("#movie-year").text(data.release_year);
    $("#movie-director").text(data.director);
    $("#movie-writer").text(data.writer);
    var stars = [];
    for (var i=0; i<3; i++) {
        if (data.stars[i] != null) {
            stars.push(data.stars[i]);
        }
    }
    $("#movie-stars").text(stars.join(", "));
    var locations = [];
    for (var i=0; i<data.locations.length; i++) {
        locations.push(data.locations[i].name);
    }
    $("#movie-locations").text(locations.join(", "));
}

function hideSplash() {
    $("#splash-page").hide();
    $("#results-page").show();
    // Map will not load properly without this line, per http://stackoverflow.com/a/3412601/1435804
    google.maps.event.trigger(map, 'resize');
    splash = false;
}

$(document).ready( function() {
    // Google Maps.
    google.maps.event.addDomListener(window, 'load', initialize);

    // Set up jQuery autocomplete.
    $("#autocomplete").autocomplete({
        autofocus: true,
        source: autoComplete,
        minLength: 2,
        select: function(event, ui) {
                fetchLocation(ui.item.id);
            }
    });

    // Carousel link handling
    $(function() {
        $(".carousel-link").click(function(e) {
            alert(e.id);
        })
    })

});