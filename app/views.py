from flask import render_template, request
from app import app, models
from json import dumps


@app.route('/', methods=["GET"])
@app.route('/index', methods=["GET"])
@app.route('/index.html', methods=["GET"])
def index():
    return render_template('index.html')


@app.route("/autocomplete", methods=["GET"])
def autocomplete():
    """
    Endpoint for autocomplete queries.
    """

    results = []
    if request.args.get("query"):
        term = "%" + request.args.get("query") + "%"
        results = models.Movie.query.filter(models.Movie.title.like(term)).all()

    # Serialize results. TODO: score results and only return top n.
    return dumps([{"id": result.id, "value": result.title} for result in results])


@app.route("/movies/<movie_id>", methods=["GET"])
def movies(movie_id):
    """
    Endpoint for movies.
    """
    movie = models.Movie.query.get_or_404(movie_id)

    locations = [{"lat": location.lat,
                 "lng": location.lng,
                 "name": location.name,
                 "fun_facts": location.fun_facts} for location in movie.locations]

    return dumps( {"title": movie.title,
                  "id": movie.id,
                  "release_year": movie.release_year,
                  "director": movie.director,
                  "writer": movie.writer,
                  "stars": [movie.actor_1, movie.actor_2, movie.actor_3],
                  "locations": locations} )