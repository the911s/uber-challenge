import os

basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

# A spot in downtown SF for locations that cannot be geocoded.
DEFAULT_LOCATION_LATITUDE = 37.774929
DEFAULT_LOCATION_LONGITUDE = -122.419416

