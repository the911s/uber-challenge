Andy Maltun Uber Web Coding Challenge Service
=============================================

Project: SF Movies

Technical track: After discussions with Andre Gordenstein, I have followed the back-end track. However, as I mentioned during our conversation, I am open to various positions. 

Technical choices
-----------------

* Framework: Flask. I use Python on a fairly regular basis and built a few small projects using Flask over a year ago. I've since used Django, but thought it would be fun to try using Flask again for this relatively simple application.
* Database: SQLite. This database seemed up to the task for the demands of the project. Concurrency was not required, nor were sophisticated indexes such as R trees. And nothing could be faster or easier. However, I am experienced with PostgreSQL databases and would likely have chosen that for something beyond a demo.
* Front-end: Plan old JavaScript. I chose to use straight JavaScript over a framework such as Backbone.js because JavaScript seemed up to the task of implementing the relatively small feature set needed here. Had I chosen the Email Service project, for instance, Backbone would have been a more natural choice. 

Trade-offs and improvements
---------------------------

This was a fun project and I would have liked to add more features, such as the ability to search by location or actor. This is obviously not difficult, but would have taken more time to implement and think through the (admittedly basic) UX issues... unfortunately with a hectic week at work, more features were not feasible. In general, things could clearly be more polished, but for a few 12-3 am nights after work, I am excited about the result.

I would also have liked to make things a bit more robust, with more tests. There are some quirky user interface issues, such as when searching for a title not in the database. There are also plenty of other bugs I'm sure I haven't found.

Other code
----------

Unfortunately I do not have much public code I can share. The code I'd most like to share has been for clients. I hope this demo gives casts my skills in a positive light. 

Resume
------

Please see my resume [here](http://andys-ubercool-sf-movies.herokuapp.com/static/resume.html) and my Github page [here](https://github.com/the911s).

Thank you for your consideration!

