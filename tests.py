import json
import os
import unittest
from config import basedir
from app import app, db
from app.models import Movie, Location
from helpers import get_or_create
from load_data import load, mock_data, geocode_locations, _geocode_location
from coverage import coverage


cov = coverage(branch=True, omit=['*/envs/*', 'tests.py'])
cov.start()


class DataPopulationTests(unittest.TestCase):
    """
    Borrows heavily from http://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-vii-unit-testing
    """

    def setUp(self):
        app.config["TESTING"] = True
        app.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite:///' + os.path.join(basedir, 'test.db')
        self.app = app.test_client()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    # # Model and data loading tests
    # def test_get_or_create(self):
    #     def get_or_create_once(name, count):
    #         _, loc = get_or_create(db.session, Location, name=name)
    #         db.session.add(loc)
    #         db.session.commit()
    #         locations = Location.query.all()
    #         assert len(locations) == count
    #     get_or_create_once("Golden Gate Bridge", 1)
    #     get_or_create_once("Golden Gate Bridge", 1)
    #     get_or_create_once("Ferry Building", 2)
    #
    # def test_add_movies_locations(self):
    #     l1 = Location(name="Golden Gate Bridge")
    #     l2 = Location(name="Twin Peaks")
    #     l3 = Location(name="Muir Woods")
    #     m1 = Movie(title="Bedazzled", release_year=2000)
    #     m2 = Movie(title="Twilight", release_year=2012)
    #     db.session.add(l1)
    #     db.session.add(l2)
    #     db.session.add(l3)
    #     db.session.add(m1)
    #     db.session.add(m2)
    #     db.session.commit()
    #     m1.locations.append(l1)
    #     m1.locations.append(l2)
    #     m2.locations.append(l1)
    #     m2.locations.append(l2)
    #     m2.locations.append(l3)
    #     # Try to add it again
    #     m2.locations.append(l3)
    #     db.session.commit()
    #     assert len(m1.locations) == 2
    #     assert len(m2.locations) == 3
    #     assert len(Location.query.filter(Location.name.like("%golden gate%")).all()) == 1
    #     assert len(Location.query.filter(Location.name.like("%twin peaks%")).all()) == 1
    #
    # def test_load_movies_json(self):
    #     load(db.session, mock_data)
    #     assert len(Movie.query.all()) == 1
    #     assert len(Location.query.all()) == 3
    #     # Test geocoding
    #     geocode_locations(db.session)
    #     loc = Location.query.filter(Location.name.like("%20th and folsom%")).first()
    #     assert abs(loc.lat - 37.758895) < 0.001
    #     assert abs(loc.lng - -122.4147242) < 0.001
    #     loc = Location.query.filter(Location.name.like("%golden gate park%")).first()
    #     assert abs(loc.lat - 37.7694208) < 0.001
    #     assert abs(loc.lng - -122.4862138) < 0.001
    #     # Try to geocode locations again; should not send HTTP request.
    #     assert _geocode_location(loc) is False

    # View tests...
    def test_index(self):
        response = self.app.get("/")
        assert response.status_code == 200
        response = self.app.get("/index")
        assert response.status_code == 200

    def test_autocomplete(self):
        m1 = Movie(title="Bedazzled", release_year=2000)
        db.session.add(m1)
        db.session.commit()
        response = json.loads(self.app.get("/autocomplete?query=be").data)
        assert response[0]["value"] == "Bedazzled"
        response = json.loads(self.app.get("/autocomplete?query=hello").data)
        assert response == []
        response = json.loads(self.app.get("/autocomplete?query=").data)
        assert response == []
        response = json.loads(self.app.get("/autocomplete").data)
        assert response == []

    def test_movie_details(self):
        load(db.session, mock_data)
        response = json.loads(self.app.get("/movies/1").data)
        assert response["title"] == "A Jitney Elopement"
        assert response["release_year"] == 1915
        assert type(response["locations"]) is list
        response = self.app.get("/movies/2")
        assert response.status_code == 404
        response = self.app.get("/movies")
        assert response.status_code == 404


if __name__ == '__main__':

    try:
        unittest.main()
    except:
        pass
    cov.stop()
    cov.save()
    print "\n\nCoverage Report:\n"
    cov.report()
    cov.erase()