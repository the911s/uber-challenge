def get_or_create(session, model, **kwargs):
    """
    Django-like get_or_create for SQLAlchemy.
    Derived from http://stackoverflow.com/a/6078058/1435804
    """
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return True, instance
    else:
        instance = model(**kwargs)
        session.add(instance)
        return False, instance
