import urllib
import urllib2
import json
from app import models, db
from helpers import get_or_create
from config import DEFAULT_LOCATION_LATITUDE, DEFAULT_LOCATION_LONGITUDE


mock_data = [[13, "692D9EFB-5673-495E-8D39-0A1B3506249B", 13, 1397770276, "881420", 1397770276, "881420", "{\n}",
               "A Jitney Elopement", "1915", "20th and Folsom Streets", None, "The Essanay Film Manufacturing Company",
               "General Film Company", "Charles Chaplin", "Charles Chaplin", "Charles Chaplin", None, None]
                 , [12, "70678098-94B4-47B9-A77B-DDCDD61DB899", 12, 1397770276, "881420", 1397770276, "881420", "{\n}",
                    "A Jitney Elopement", "1915", "Golden Gate Park",
                    "During San Francisco's Gold Rush era, the Park was part of an area designated as the \"Great Sand Waste\". ",
                    "The Essanay Film Manufacturing Company", "General Film Company", "Charles Chaplin",
                    "Charles Chaplin", "Charles Chaplin", None, None],
             # Handle None location name
             [12, "70678098-94B4-47B9-A77B-DDCDD61DB899", 12, 1397770276, "881420", 1397770276, "881420", "{\n}",
                    "A Jitney Elopement", "1915", None,
                    "During San Francisco's Gold Rush era, the Park was part of an area designated as the \"Great Sand Waste\". ",
                    "The Essanay Film Manufacturing Company", "General Film Company", "Charles Chaplin",
                    "Charles Chaplin", "Charles Chaplin", None, None],
             # Handle unicode location name
             [12, "70678098-94B4-47B9-A77B-DDCDD61DB899", 12, 1397770276, "881420", 1397770276, "881420", "{\n}",
                    "A Jitney Elopement", "1915", u"Cata\xc3o",
                    "During San Francisco's Gold Rush era, the Park was part of an area designated as the \"Great Sand Waste\". ",
                    "The Essanay Film Manufacturing Company", "General Film Company", "Charles Chaplin",
                    "Charles Chaplin", "Charles Chaplin", None, None]]


def load(session, data):

    fields = ["title",
              "release_year",
              "locations",
              "fun_facts",
              "production_company",
              "distributor",
              "director",
              "writer",
              "actor_1",
              "actor_2",
              "actor_3"]

    print "Processing movies and locations..."
    for row in data:
        # Loop through the row and pack the info into a dict.
        entry = {}
        for i, col in enumerate(row[8:]):
                entry[fields[i]] = col

        # Some locations appear to be "None"... skip the entire row since they add no information to a
        # film location app.
        if entry["locations"] is None:
            continue

        # First check if the location exists in the DB, and if not add it.
        exists, location = get_or_create(db.session, models.Location, name=entry["locations"])
        if not exists:
            location.fun_facts = entry["fun_facts"]
            session.add(location)
            session.commit()

        # Next check if the movie exists. If not, create it. Then add the location.
        exists, movie = get_or_create(db.session,
                                      models.Movie,
                                      title=entry["title"])
        if not exists:
            movie.actor_1 = entry["actor_1"]
            movie.actor_2 = entry["actor_2"]
            movie.actor_3 = entry["actor_3"]
            movie.release_year = entry["release_year"]
            movie.writer = entry["writer"]
            movie.director = entry["director"]
            movie.production_company = entry["production_company"]
            movie.distributor = entry["distributor"]
        # Add the location
        movie.locations.append(location)
        session.add(movie)
        session.commit()


def _geocode_location(location):
    """
    Geocode a single address or place using Google geocoding API.
    """
    # Don't hit geocoding API if we already know the location.
    if abs(location.lat - DEFAULT_LOCATION_LATITUDE) < 0.001 and \
                    abs(location.lng - DEFAULT_LOCATION_LONGITUDE) < 0.001:
        address = location.name + ", San Francisco, CA"
        url = "http://maps.googleapis.com/maps/api/geocode/json?address=" + urllib.quote_plus(address.encode("utf-8"))
        response = urllib2.urlopen(url)
        data = json.loads(response.read())
        if data["results"]:
            return data["results"][0]["geometry"]["location"]["lat"], data["results"][0]["geometry"]["location"]["lng"]
        else:
            return DEFAULT_LOCATION_LATITUDE, DEFAULT_LOCATION_LONGITUDE
    else:
        return False


def geocode_locations(session):
    """
    Go thru the database and enter lat and lon info for each film location.
    """
    locations = models.Location.query.all()

    for location in locations:
        print "Geocoding", location.name
        result = _geocode_location(location)
        if result:
            location.lat = result[0]
            location.lng = result[1]
            session.add(location)
            session.commit()
        else:
            print "Location was already geocoded."



if __name__ == '__main__':
    # Load the movie database from data.sfgov.org into a SQL database.
    # http://data.sfgov.org/resource/yitu-d5am.json
    data = json.load(open("data.json"))
    data = data["data"]
    load(db.session, data)
    geocode_locations(db.session)